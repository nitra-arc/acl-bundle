<?php

namespace Nitra\AclBundle\Doctrine;

use Doctrine\ORM\Proxy\ProxyFactory as BaseProxyFactory;

class ProxyFactory extends BaseProxyFactory
{
    /** @var ProxyGenerator */
    protected $proxyGenerator;

    /** @var \Doctrine\Common\Persistence\Mapping\ClassMetadataFactory */
    protected $metadataFactory;

    /** @var boolean Whether to automatically (re)generate proxy classes. */
    protected $autoGenerate;

    /** @var \Doctrine\Common\Proxy\ProxyDefinition[] */
    protected $definitions = array();

    /**
     * {@inheritdoc}
     */
    public function __construct(EntityManager $em, $proxyDir, $proxyNs, $autoGenerate = false)
    {
        parent::__construct($em, $proxyDir, $proxyNs, $autoGenerate);

        $this->metadataFactory = $em->getMetadataFactory();
        $this->proxyGenerator  = new ProxyGenerator($proxyDir, $proxyNs);
        $this->autoGenerate    = (int) $autoGenerate;
    }

    /**
     * {@inheritdoc}
     */
    public function generateProxyClasses(array $classes, $proxyDir = null)
    {
        $generated = 0;

        foreach ($classes as $class) {
            if ($this->skipClass($class)) {
                continue;
            }

            $proxyFileName = $this->proxyGenerator->getProxyFileName($class->getName(), $proxyDir);

            $this->proxyGenerator->generateProxyClass($class, $proxyFileName);

            $generated += 1;
        }

        return $generated;
    }

    /**
     * {@inheritdoc}
     */
    public function getProxy($className, array $identifier)
    {
        $definition = isset($this->definitions[$className])
            ? $this->definitions[$className]
            : $this->getProxyDefinition($className);
        $fqcn       = $definition->proxyClassName;
        $proxy      = new $fqcn($definition->initializer, $definition->cloner);

        foreach ($definition->identifierFields as $idField) {
            if (!isset($identifier[$idField])) {
                throw OutOfBoundsException::missingPrimaryKeyValue($className, $idField);
            }

            $definition->reflectionFields[$idField]->setValue($proxy, $identifier[$idField]);
        }

        return $proxy;
    }

    /**
     * {@inheritdoc}
     */
    protected function getProxyDefinition($className)
    {
        $classMetadata = $this->metadataFactory->getMetadataFor($className);
        $realClassName = $classMetadata->getName(); // aliases and case sensitivity

        $this->definitions[$realClassName] = $this->createProxyDefinition($realClassName);
        $proxyClassName                    = $this->definitions[$realClassName]->proxyClassName;

        if (!class_exists($proxyClassName, false)) {
            $fileName = $this->proxyGenerator->getProxyFileName($realClassName);

            switch ($this->autoGenerate) {
                case self::AUTOGENERATE_NEVER:
                    require $fileName;
                    break;

                case self::AUTOGENERATE_FILE_NOT_EXISTS:
                    if (!file_exists($fileName)) {
                        $this->proxyGenerator->generateProxyClass($classMetadata, $fileName);
                    }
                    require $fileName;
                    break;

                case self::AUTOGENERATE_ALWAYS:
                    $this->proxyGenerator->generateProxyClass($classMetadata, $fileName);
                    require $fileName;
                    break;

                case self::AUTOGENERATE_EVAL:
                    $this->proxyGenerator->generateProxyClass($classMetadata, false);
                    break;
            }
        }

        return $this->definitions[$realClassName];
    }
}