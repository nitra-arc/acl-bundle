<?php

namespace Nitra\AclBundle\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginListener
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Doctrine\Common\Annotations\AnnotationReader
     */
    protected $reader;

    /**
     * Constructs a new instance of SecurityListener.
     *
     * @param \Symfony\Component\Security\Core\SecurityContext  $session The session context
     * @param \Doctrine\ORM\EntityManager                       $em
     */
    public function __construct(Session $session, $em)
    {
        $this->session = $session;
        $this->em      = $em;
        $this->reader  = new \Doctrine\Common\Annotations\AnnotationReader();
    }

    /**
     * Invoked after a successful login.
     *
     * @param InteractiveLoginEvent $event The event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        // Проходимо всі таблиці
        $allEntities = $this->em->getMetadataFactory()->getAllMetadata();
        foreach ($allEntities as $entity) {

            // Отримуємо всі поля таблиці
            $reflectionProperties = $entity->getReflectionProperties();
            foreach ($reflectionProperties as $property) {

                // Читаємо анотації кожного поля
                $annotations = $this->reader->getPropertyAnnotations($property);
                foreach ($annotations as $annot) {

                    // Якщо знайдено Acl анотацію
                    if ($annot instanceof \Nitra\AclBundle\Annotation\Acl) {

                        // Знаходимо повязані ідентифікатори
                        $rels = $this->em->createQueryBuilder()
                            ->select('s.id')
                            ->from($entity->getName(), 's', 's.id')
                            ->innerJoin('s.users', 'u')
                            ->where('u.id = :user')
                            ->setParameter('user', $user->getId())
                            ->getQuery()
                            ->getArrayResult();

                        // Записуємо в спільний кеш
                        if (!empty($rels)) {
                            $this->session->set($entity->getName(), array_keys($rels));
                        }
                    }
                }
            }
        }
    }
}