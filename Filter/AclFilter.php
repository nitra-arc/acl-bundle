<?php

namespace Nitra\AclBundle\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Фільтрація запитів до БД
 */
class AclFilter extends SQLFilter
{
    /**
     * {@inheritdoc}
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (isset($_SESSION["_sf2_attributes"])) {
            $reader = new \Doctrine\Common\Annotations\AnnotationReader();

            // обмеження для користувача берем напряму з сесії
            $session = $_SESSION["_sf2_attributes"];

            // Отримуємо всі поля таблиці
            $reflectionProperties = $targetEntity->getReflectionProperties();
            foreach ($reflectionProperties as $property) {

                // Читаємо анотації кожного поля
                $annotations = $reader->getPropertyAnnotations($property);
                foreach ($annotations as $annot) {

                    // Якщо знайдено Acl анотацію
                    if ($annot instanceof \Nitra\AclBundle\Annotation\Acl) {
                        // Перевіряємо, чи э у сесії обмеження
                        if (isset($session[$targetEntity->getName()])) {
                            return $targetTableAlias . '.id IN (' . implode(",", $session[$targetEntity->getName()]) . ')';
                        }
                    }
                }
            }
        }

        return '';
    }
}