# Nitra. Acl bundle

### Installing

```bash
composer require nitra/acl-bundle
```

```php
<?php
// app/AppKernel.php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    // ....

    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = array(
            // ....
            new Nitra\AclBundle\NitraAclBundle(),
            // ....
        );
        // ....

        return $bundles;
    }

    // ....
}
```

### Using

```php
<?php
// src/.../Entity/Entity.php

namespace ...\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nitra\AclBundle\Annotation\Acl;

/**
 * @ORM\Table
 * @ORM\Entity
 */
class Entity
{
    // fields

    /**
     * @Acl
     * @ORM\ManyToMany(targetEntity="User", inversedBy="entities")
     * @ORM\JoinTable(name="entity_user")
     */
    protected $users;

    // getters, setters, constuct...
}
```